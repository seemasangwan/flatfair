import Vue from 'vue'
import Router from 'vue-router'
import performance from '@/components/performance'
import settings from '@/components/settings'
import FlatbondList from '@/components/FlatbondList'
import axios from 'axios'

Vue.use(axios)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'FlatbondList',
      component: FlatbondList
    },
    {
      path: '/settings',
      name: 'settings',
      component: settings
    },
    {
      path: '/performance',
      name: 'performance',
      component: performance
    }
  ]
})

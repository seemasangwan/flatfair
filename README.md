# test-flatfair

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# Project Detail for Flatfair

This is a VueJs Project to demostrate the task given by Flatfair.

To run this project, vuejs installation is required if not exist in the system. \please follow the following instructions:


# Installation
Prerequisites: Node.js (>=6.x, 8.x preferred), npm version 3+ and Git.

$ npm install -g vue-cli


# Git
Clone the repo from Git.

# File structure

flatfair---|
           |
           ------test-flatfair(vue project files)
           |
           |
           -------data(local json file)
           

# Created the local API 
npm install -g json-server

Json path:
cd /flatfair/data/db

# install dependencies (To run the project first need to install dependencies itw will create node_modules folder)
npm install

# serve with hot reload at localhost:8080(in browser got to localhost:8080 after running following command )
npm run dev

axios is also used as dependency.

# Project details

There are 3 main component in folder src
FLatboundList.vue
setting.vue
performance.vue

The Navbar is fixed for all the components and created in index.html

# project decription
The project is build with following:
ajax
bootstrap
jquery

The CDN of above added in index.html

The route also define for all the pages in folder router/main.js


# Code architect

Firstly did the setup for the Vuejs.

1) Started with navbar and route for all the pages.
2) Then created local API in form of json and tested it.
3) Added more options to navbar and CSS like hover effects etc.
4) Started component FLatbondList, using bootstrap made grid to extcute the API in frontend. Using jquery added functionality to the component. Add filter(just the frontend not function its work when click on filter button) and  new flatbond (its adding dummy test data everytime you click the button its not saved in Json)
5) Next step to create settings component, created a form to update the existing data. The search is done by name if name exist (can try Silver, red, White) the other test field will be auto populated with the respective values. If its name does not exist or name field is empty it will give error. The update button is non functional.
6) Next compenent is performance which is empty.
7) The project is fully responsive and added some media queries to adjust css in different screnn sizes.













# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
